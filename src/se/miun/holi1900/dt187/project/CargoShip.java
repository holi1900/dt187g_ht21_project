package se.miun.holi1900.dt187.project;
/**
* <h1>CargoShip</h1>
This class manages the cargo ship that inherits from ship. holds a list of all cargo in the ship

* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CargoShip extends Ship{
    private ArrayList<Cargo> shipLoad = new ArrayList<Cargo>();
   
    public CargoShip(String name, int maxWeight, int crew) {
        super(name, maxWeight, crew, ShipType.CARGO);
    }

    public ArrayList<Cargo> getAllCargo(){
        return shipLoad;
    }
    /**
     * adds a cargo to the list of cargo if cargo it not null
     * @param cargo
     */
    public void addCargo(Cargo cargo) throws OverloadException{
        if(cargo !=null){
            int intendedWeight = getCurrentWeight() + (int)cargo.getWeight();
            
            if(intendedWeight > maxWeight){
                throw new OverloadException("The ship does not have enough capacity left for this load.");
            }
            else{
                shipLoad.add(cargo);
            }
        }
    }
    /**
     * 
     * @return the total weight of all the cargo
     */
    private float getWeight(){
        float totalLoadWeight = 0;
        for(Cargo c: shipLoad){
            totalLoadWeight += c.getWeight();
        }
        return totalLoadWeight;
    }

    public void ClearCargo(){
        shipLoad.clear();
    }

    public boolean readFromFile(String filename) throws OverloadException{
        try{
            Scanner fileScanner = new Scanner(new File(filename));
            String cargoName = fileScanner.nextLine();
            float weight = fileScanner.nextFloat();
            addCargo(new Cargo(cargoName, weight));

            fileScanner.close();
            return true;
        }
        catch(FileNotFoundException e){
            return false;
        }

    }
    /**
     * returns total weight of crew and Cargo
     */
    @Override
    public int getCurrentWeight() {
        int totalWeight = crew*crewWeight + (int)getWeight();
        return totalWeight;
    }
    /**
     * returns ship name and current weight
     */
    @Override
    public String toString() {
        
        return "Name: " + name + " current weight: " + getCurrentWeight();
    }    

}
