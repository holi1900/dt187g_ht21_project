package se.miun.holi1900.dt187.project;
/**
 * This is an abstract class for ship with holds name, max weight and crew
 */

public abstract class Ship {
    public static final int passengerweight = 150;
    public static final int crewWeight = 100;

    protected String name;
    protected int maxWeight;
    protected int crew;
    private ShipType type;
    public Ship(String name, int maxWeight, int crew, ShipType type) {
        this.name = name;
        this.maxWeight = maxWeight;
        this.crew = crew;
        this.type = type;
    }
    public String getName() {
        return name;
    }
    
    public int getMaxWeight() {
        return maxWeight;
    }
    
    public int getCrew() {
        return crew;
    }
   
    public ShipType getType() {
        return type;
    }
    public abstract int getCurrentWeight();
    public abstract String toString();

    

    
}
