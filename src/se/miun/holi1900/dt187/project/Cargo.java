package se.miun.holi1900.dt187.project;
/**
* <h1>Cargo</h1>
* This Class handles the cargo and it properties, name, weight and if it is heigh risk
* 
* 
* 
* 
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/

public class Cargo {
    private String name;
    private float weight;
    boolean highRisk;
    public Cargo(String name, float weight) {
        this.name = name;
        this.weight = weight;
        highRisk = false;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public float getWeight() {
        return weight;
    }
    public void setWeight(float weight) {
        this.weight = weight;
    }
    public boolean isHighRisk() {
        return highRisk;
    }
    public void setHighRisk(boolean highRisk) {
        this.highRisk = highRisk;
    }

    public String toString(){
        return name + " [" + weight + "]";
    }
    

}
