package se.miun.holi1900.dt187.project;

/**
* <h1>GUI</h1>
* This class handles the presentation of ship information and Cargo list in a graphic display
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;

import javax.swing.filechooser.*;
import javax.swing.filechooser.FileFilter;

public class GUI extends JFrame implements ActionListener{
	// Inastansvariabler (alla grafiska komponenter samt fraktfartyget)
	private JPanel shipPanel;
	private JPanel cargoPanel;
	private JTextField shipNameTextField;
	private JTextField crewTextField;
	private JTextField maxWeightTextField;
	private JTextField currentWeightTextField;
	private JList cargoList;
	private CargoShip cargoShip;

	public GUI(String title) {
		// Sätt titel på fönstret
		super(title);

		// Vad ska ske när vi stänger fönstret?
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		// Centrera fönstret på skärmen
		setLocationRelativeTo(null);

		// Ange vilken layout som ska användas i fönstret
		setLayout(new BorderLayout());

		// Initiera alla komponenter
		initComponents();

		// Uppdatera GUI med information från fraktfartyget
		updateShipInfo();

		// Sätt storleken på fönstret
		setSize(300, 250);

		// Gör fönstret synligt
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new GUI("Cargo ship info"));
	}

	private void initComponents() {
		makeMenu();

		cargoPanel = new JPanel(new BorderLayout(5, 2));
		cargoList = new JList<Cargo>();
		cargoShip = getDefaultCargoShip();
		
		JLabel nameLabel = new JLabel("Ship name");
		JLabel crewLabel = new JLabel("Crew");
		JLabel weightLabel = new JLabel("Max weight");
		JLabel currentWeightLabel = new JLabel("Current weight");

		shipNameTextField = new JTextField();
		crewTextField = new JTextField();
		maxWeightTextField = new JTextField();
		currentWeightTextField = new JTextField();

		shipPanel = new JPanel();
		shipPanel.setLayout(new GridLayout(4,2));
		shipPanel.add(nameLabel);
		shipPanel.add(crewLabel);
		shipPanel.add(shipNameTextField);
		shipPanel.add(crewTextField);
		shipPanel.add(weightLabel);
		shipPanel.add(currentWeightLabel);
		shipPanel.add(maxWeightTextField);
		shipPanel.add(currentWeightTextField);
		add(shipPanel, BorderLayout.NORTH);

		cargoPanel.add(new JLabel("Cargo"), BorderLayout.NORTH);
		cargoPanel.add(new JScrollPane(cargoList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
		add(cargoPanel, BorderLayout.CENTER);
	}

	private void updateShipInfo() {
		if (cargoShip == null) {
			return;
		}
		shipNameTextField.setText(cargoShip.getName());
		crewTextField.setText(String.valueOf(cargoShip.getCrew()));
		maxWeightTextField.setText(String.valueOf(cargoShip.getMaxWeight()));
		currentWeightTextField.setText(String.valueOf(cargoShip.getCurrentWeight()));

		updateCargoList();
	}

	private void updateCargoList(){
		cargoList.setListData(cargoShip.getAllCargo().toArray());
	}

	private CargoShip getDefaultCargoShip(){
		CargoShip ship = new CargoShip("Cargo express", 3000, 10);
		try{
			ship.addCargo(new Cargo("Food stuff", 300));
			ship.addCargo(new Cargo("Petrol", 400));
			ship.addCargo(new Cargo("Cloths", 500));
			ship.addCargo(new Cargo("Electronics", 300));
			ship.addCargo(new Cargo("Chemicals", 300));
		}
		catch (OverloadException e) {
			JOptionPane.showMessageDialog(shipPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		return ship;
	}

	private void makeMenu(){
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Cargo");
		menuBar.add(menu);
		JMenuItem menuItem = new JMenuItem("Load");
		menu.add(menuItem);
		menuItem.addActionListener(this);
		menuItem = new JMenuItem("Clear all");
		menu.add(menuItem);
		menuItem.addActionListener(this);
		setJMenuBar(menuBar);
	}

	private void loadCargoFromFile(){
		FileFilter cargoFilter = new FileNameExtensionFilter("Cargo files", "cargo");

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.addChoosableFileFilter(cargoFilter);
		int option = chooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) {
			String name = chooser.getSelectedFile().getName();
			String path = chooser.getSelectedFile().getAbsolutePath();
			try{
				if(cargoShip.readFromFile(path)){
					JOptionPane.showMessageDialog(shipPanel, "The file has been read","Confirmation", JOptionPane.INFORMATION_MESSAGE);
					updateShipInfo();
				} else{
					String message = "The choosen file could not be opened";
					JOptionPane.showMessageDialog(shipPanel, message, "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			catch(OverloadException e){
				JOptionPane.showMessageDialog(shipPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(shipPanel, "You have choosen to cancel.", "Error", JOptionPane.ERROR_MESSAGE);
		}
				

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String itemText = e.getActionCommand();

		if(itemText.equals("Load")){
			loadCargoFromFile();

		}
		else if(itemText.equals("Clear all")){
			cargoShip.ClearCargo();
			updateShipInfo();

		}
		
	}

	
}