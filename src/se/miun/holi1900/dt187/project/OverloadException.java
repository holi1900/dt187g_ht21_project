package se.miun.holi1900.dt187.project;

public class OverloadException extends Exception{

    public OverloadException() {
        super();
    }
    
    public OverloadException(String message) {
        super(message);
    }
}
