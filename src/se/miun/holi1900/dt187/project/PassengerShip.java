package se.miun.holi1900.dt187.project;

/**
 * Class PassengerShip manages the passengership type and holds information of ship name, crew,
 * nr of passengers, current weight and max passengers
 */

public class PassengerShip extends Ship{
    private int maxPassengers;
    private int passengers;

    public PassengerShip(String name, int maxWeight, int crew, int maxPassengers) {
        super(name, maxWeight, crew, ShipType.PASSENGER);
        this.maxPassengers = maxPassengers;
        this.passengers = 0;
    }

    public void setPassenger(int passenger) throws OverloadException {
        if(passenger < 0){
            throw new OverloadException("The number of passengers must be greater than or equals to zero.");
        }
        else if(passenger > maxPassengers){
            throw new OverloadException("The number of passengers are more that the maximum number allowed.");
        }
        else{
            this.passengers = passenger;
        }
    }

    @Override
    public int getCurrentWeight() {
        return crew*crewWeight + passengers*passengerweight;
    }
    /**
     * returns passengers name, current wight and number of passengers
     */
    @Override
    public String toString() {
        return "name: " + name + "current weight: " + getCurrentWeight() + "Number of passengers: " + passengers;
    }
    
}
