package se.miun.holi1900.dt187.project;
/**
* This class runs a program that displays cargoship information on a GUI
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/

public class Uppgift5 {
    public static void main(String[] args) {
		GUI g = new GUI("Cargo Ship");
		g.setVisible(true);
	}
}
